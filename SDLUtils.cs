using System;
using osx = SDL2.osx.SDL;
using win = SDL2.win.SDL;

namespace SyscomGame
{
    public class SDLException : Exception
    {
        public SDLException(string message) : base(message)
        {
            
        }
    }
    class SDLUtils
    {
        OS _os;
        public SDLUtils(OS os)
        {
            _os = os;
        }

        public int CheckSDLResult(int sdlResult, string message)
        {
            if(sdlResult < 0){
                string error = string.Empty;
                if(_os == OS.OSX)
                {
                    error = osx.SDL_GetError();
                }
                else if(_os == OS.WIN)
                {
                    error = win.SDL_GetError();
                }
                throw new SDLException(message+ " SDLError: "+error);
            }

            return sdlResult;
        }

        public IntPtr CheckSDLResult(IntPtr sdlResult, string message)
        {
            if(sdlResult == null){
                string error = string.Empty;
                if(_os == OS.OSX)
                {
                    error = osx.SDL_GetError();
                }
                else if(_os == OS.WIN)
                {
                    error = win.SDL_GetError();
                }
                throw new SDLException(message+ " SDLError: "+error);
            }

            return sdlResult;
        }
    }
}