namespace SyscomGame
{
    public class Color{
        public byte R { get; set;}
        public byte G { get; set;}
        public byte B { get; set;}
        public byte Opacity { get; set;}
    }
}