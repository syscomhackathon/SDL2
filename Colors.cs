namespace SyscomGame
{
    public class Colors {
        public static readonly Color Red = new Color {R=255,G=0,B=0,Opacity=255};
        public static readonly Color Green = new Color {R=0,G=255,B=0,Opacity=255};
        public static readonly Color Blue = new Color {R=0,G=0,B=255,Opacity=255};
        public static readonly Color White = new Color {R=255,G=255,B=255,Opacity=255};
        public static readonly Color Black = new Color {R=0,G=0,B=0,Opacity=255};
    }
}