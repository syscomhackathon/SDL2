using System;
using osx = SDL2.osx.SDL;
using win = SDL2.win.SDL;

namespace SyscomGame
{
    public enum OS {
        OSX,
        WIN
    }

    public class GameWindow : IDisposable
    {
        public Color BackgroundColor { get; set; }
        IntPtr _windowSurface;
        IntPtr _gameWindow;
        IntPtr _renderer;
        SDLUtils _utils;
        OS _os;
        int _width = 0;
        int _height = 0;
        string _title = string.Empty;
        public GameWindow(string title, int width, int height, OS os)
        {
            BackgroundColor = Colors.Black;
            _title = title;
            _width = width;
            _height = height;
            _os = os;
            _utils = new SDLUtils(os);

            switch(os){
                case OS.OSX:
                    InitMac();
                    break;
                case OS.WIN:
                    InitWin();                
                    break;
            }
        }

        public void RenderBegin(){
            switch(_os){
                case OS.OSX:
                    _utils.CheckSDLResult(osx.SDL_SetRenderDrawColor(_renderer,BackgroundColor.R,BackgroundColor.G,BackgroundColor.B,BackgroundColor.Opacity),"SDL_SetRenderDrawColor failed.");
                    _utils.CheckSDLResult(osx.SDL_RenderClear(_renderer),"SDL_RenderClear failed");
                    break;
                case OS.WIN:
                    _utils.CheckSDLResult(win.SDL_SetRenderDrawColor(_renderer,BackgroundColor.R,BackgroundColor.G,BackgroundColor.B,BackgroundColor.Opacity),"SDL_SetRenderDrawColor failed.");
                    _utils.CheckSDLResult(win.SDL_RenderClear(_renderer),"SDL_RenderClear failed");
                    break;
            }
        }

        public void RenderEnd(){
            switch(_os){
                case OS.OSX:
                    osx.SDL_RenderPresent(_renderer);
                    break;
                case OS.WIN:
                    win.SDL_RenderPresent(_renderer);
                    break;
            }
        }

        public void DrawCircle(int x0, int y0, int radius, Color color)
        {
            int x = radius-1;
            int y = 0;
            int dx = 1;
            int dy = 1;
            int err = dx - (radius << 1);

            while (x >= y)
            {
                DrawPoint(x0 + x, y0 + y, color);
                DrawPoint(x0 + y, y0 + x, color);
                DrawPoint(x0 - y, y0 + x, color);
                DrawPoint(x0 - x, y0 + y, color);
                DrawPoint(x0 - x, y0 - y, color);
                DrawPoint(x0 - y, y0 - x, color);
                DrawPoint(x0 + y, y0 - x, color);
                DrawPoint(x0 + x, y0 - y, color);

                if (err <= 0)
                {
                    y++;
                    err += dy;
                    dy += 2;
                }
                if (err > 0)
                {
                    x--;
                    dx += 2;
                    err += (-radius << 1) + dx;
                }
            }
        }




        public void DrawRect(int x, int y, int width, int height, Color color){
            switch(_os){
                case OS.OSX:
                    osx.SDL_Rect osxRect = new osx.SDL_Rect();
                    osxRect.x = x;osxRect.y = y;osxRect.w = width;osxRect.h = height;
                    osx.SDL_SetRenderDrawColor(_renderer,color.R,color.G,color.B,color.Opacity);
                    osx.SDL_RenderDrawRect(_renderer,ref osxRect);
                    break;
                case OS.WIN:
                    win.SDL_Rect winRect = new win.SDL_Rect();
                    winRect.x = x;winRect.y = y;winRect.w = width;winRect.h = height;
                    win.SDL_SetRenderDrawColor(_renderer,color.R,color.G,color.B,color.Opacity);
                    win.SDL_RenderDrawRect(_renderer,ref winRect);
                    break;
            }
        }

        public void FillRect(int x, int y, int width, int height, Color color){
            switch(_os){
                case OS.OSX:
                    osx.SDL_Rect osxRect = new osx.SDL_Rect();
                    osxRect.x = x;osxRect.y = y;osxRect.w = width;osxRect.h = height;
                    osx.SDL_SetRenderDrawColor(_renderer,color.R,color.G,color.B,color.Opacity);
                    osx.SDL_RenderFillRect(_renderer,ref osxRect);
                    break;
                case OS.WIN:
                    win.SDL_Rect winRect = new win.SDL_Rect();
                    winRect.x = x;winRect.y = y;winRect.w = width;winRect.h = height;
                    win.SDL_SetRenderDrawColor(_renderer,color.R,color.G,color.B,color.Opacity);
                    win.SDL_RenderFillRect(_renderer,ref winRect);
                    break;
            }
        }
        public void DrawLine(int x1, int y1, int x2, int y2, Color color){
            switch(_os){
                case OS.OSX:
                    osx.SDL_SetRenderDrawColor(_renderer,color.R,color.G,color.B,color.Opacity);
                    osx.SDL_RenderDrawLine(_renderer,x1,y1,x2,y2);
                    break;
                case OS.WIN:
                    win.SDL_SetRenderDrawColor(_renderer,color.R,color.G,color.B,color.Opacity);
                    win.SDL_RenderDrawLine(_renderer,x1,y1,x2,y2);
                    break;
            }
        }

        public void DrawPoint(int x, int y, Color color){
            switch(_os){
                case OS.OSX:
                    osx.SDL_SetRenderDrawColor(_renderer,color.R,color.G,color.B,color.Opacity);
                    osx.SDL_RenderDrawPoint(_renderer,x,y);
                    break;
                case OS.WIN:
                    win.SDL_SetRenderDrawColor(_renderer,color.R,color.G,color.B,color.Opacity);
                    win.SDL_RenderDrawPoint(_renderer,x,y);
                    break;
            }
        }

        public void Dispose()
        {
            switch(_os){
                case OS.OSX:
                    DisposeMac();
                    break;
                case OS.WIN:
                    DisposeWin();                
                    break;
            }
        }

        void DisposeMac(){
            //Destroy window
            osx.SDL_DestroyWindow(_gameWindow);
            _gameWindow = IntPtr.Zero;

            //Quit SDL subsystems
            osx.SDL_Quit();
        }

        void DisposeWin(){
            win.SDL_DestroyWindow(_gameWindow);
            _gameWindow = IntPtr.Zero;

            //Quit SDL subsystems
            win.SDL_Quit();
        }

        void InitMac(){
            osx.SDL_SetMainReady();
            _utils.CheckSDLResult(osx.SDL_Init(osx.SDL_INIT_VIDEO),"SDL_Init failed.");
            
            _gameWindow = osx.SDL_CreateWindow(_title,osx.SDL_WINDOWPOS_CENTERED,osx.SDL_WINDOWPOS_CENTERED,_width,_height,osx.SDL_WindowFlags.SDL_WINDOW_SHOWN);
            _utils.CheckSDLResult(_gameWindow,"SDL_CreateWindow failed.");
            
            _renderer = osx.SDL_CreateRenderer(_gameWindow,-1,osx.SDL_RendererFlags.SDL_RENDERER_SOFTWARE);
            _utils.CheckSDLResult(_renderer,"SDL_CreateRenderer failed.");

            _windowSurface = osx.SDL_GetWindowSurface(_gameWindow);
            _utils.CheckSDLResult(_windowSurface,"SDL_GetWindowSurface failed.");
        }

        void InitWin(){
            win.SDL_SetMainReady();
            _utils.CheckSDLResult(win.SDL_Init(win.SDL_INIT_VIDEO),"SDL_Init failed.");
            
            _gameWindow = win.SDL_CreateWindow(_title,win.SDL_WINDOWPOS_CENTERED,win.SDL_WINDOWPOS_CENTERED,_width,_height,win.SDL_WindowFlags.SDL_WINDOW_SHOWN);
            _utils.CheckSDLResult(_gameWindow,"SDL_CreateWindow failed.");
            
            _renderer = win.SDL_CreateRenderer(_gameWindow,-1,win.SDL_RendererFlags.SDL_RENDERER_SOFTWARE);
            _utils.CheckSDLResult(_renderer,"SDL_CreateRenderer failed.");

            _windowSurface = win.SDL_GetWindowSurface(_gameWindow);
            _utils.CheckSDLResult(_windowSurface,"SDL_GetWindowSurface failed.");
        }
    }
}